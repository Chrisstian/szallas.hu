<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static whereIn(string $string, array $companyIds)
 * @method static create(array $validatedData)
 * @method static groupBy(string $string)
 * @method static where(string $string, int $companyId)
 */
class Company extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'company';

    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'registration_number',
        'foundation_date',
        'country',
        'zip_code',
        'city',
        'street_address',
        'latitude',
        'longitude',
        'owner',
        'number_of_employee',
        'activity',
        'active',
        'email',
        'password'
    ];
}
