<?php

namespace App\Handlers;

use config\Constant\Company as CompanyConst;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RuntimeException;

class CompanyHandler {

    /**
     * Searching request parameter validation
     *
     * @param object $parameters
     * @return void
     */
    public function searchingParameterValidation(object $parameters): void {
        if(isset($parameters->company_ids) === false || is_array($parameters->company_ids) === false) {
            throw new RuntimeException(CompanyConst::INVALID_PARAMETERS_ERROR_MSG);
        }
        if($this->checkCompanyIds($parameters->company_ids) === false) {
            throw new RuntimeException(CompanyConst::INVALID_PARAMETERS_ERROR_MSG);
        }
    }

    /**
     * Check company ids parameter
     *
     * @param array $companyIds
     * @return bool
     */
    private function checkCompanyIds(array $companyIds): bool {
        foreach($companyIds as $value) {
            if(is_int($value) === false) {
                return false;
            }
        }

        return true;
    }

    /**
     * Validate company request data on created
     *
     * @param Request $request
     * @return array
     */
    public function newCompanyDataValidation(Request $request): array {
        return $request->validate([
            'name' => 'required|string|min:5|max:255',
            'registration_number' => 'required|string|unique:company|min:11|max:11',
            'foundation_date' => 'required|date',
            'country' => 'required|string|min:4|max:128',
            'zip_code' => 'required|string|min:4|max:16',
            'city' => 'required|string|min:2|max:128',
            'street_address' => 'required|string|min:5|max:255',
            'latitude' => 'required|string|min:5|max:20',
            'longitude' => 'required|string|min:5|max:20',
            'owner' => 'required|string|min:5|max:128',
            'number_of_employee' => 'required|integer',
            'activity' => 'required|string|min:2|max:64',
            'active' => 'required|boolean',
            'email' => 'required|string|unique:company|email:rfc,filter',
            'password' => 'required|string|min:3|max:128'
        ]);
    }

    /**
     * Validate company request data on update
     *
     * @param int $companyId
     * @param Request $request
     * @return array
     */
    public function updateCompanyDataValidation(int $companyId, Request $request): array {
        return $request->validate([
            'name' => 'required|string|min:5|max:255',
            'registration_number' => 'required|string|unique:company,registration_number,'.$companyId.'|min:11|max:11',
            'foundation_date' => 'required|date',
            'country' => 'required|string|min:4|max:128',
            'zip_code' => 'required|string|min:4|max:16',
            'city' => 'required|string|min:2|max:128',
            'street_address' => 'required|string|min:5|max:255',
            'latitude' => 'required|string|min:5|max:20',
            'longitude' => 'required|string|min:5|max:20',
            'owner' => 'required|string|min:5|max:128',
            'number_of_employee' => 'required|integer',
            'activity' => 'required|string|min:2|max:64',
            'active' => 'required|boolean',
            'email' => 'required|string|unique:company,email,'.$companyId.'|email:rfc,filter',
            'password' => 'required|string|min:3|max:128'
        ]);
    }

    /**
     * Generate companies table by activity
     *
     * @return string
     */
    public function createCompanyTableByActivity(): string {
        $html = '<!DOCTYPE html>
                    <html>
                        <head>
                        </head>
                        <body>
                            <table style="border-collapse: collapse; width: 100%;">';

        $activities = DB::table('company')->select('activity')->distinct('activity')->orderBy('activity', 'ASC')->get();
        if(count($activities) === 0) {
            throw new RuntimeException(CompanyConst::ACTIVITIES_NOT_FOUND_ERROR_MSG);
        }

        $html .= '<tr>';
        foreach($activities as $activity) {
            $html .= '<th style="border: 1px solid #dddddd;">'.$activity->activity.'</th>';
        }
        $html .= '</tr>';

        $companies = DB::table('company')->select('name', 'activity')->get();
        foreach($companies as $company) {
            $html .= '<tr>';

            foreach ($activities as $activity) {
                $html .= '<td style="border: 1px solid #dddddd;">' .(($activity->activity === $company->activity) ? $company->name : "" ). '</td>';
            }

            $html .= '</tr>';
        }

        $html .= '</table>
                  </body>
                  </html>';

        return $html;
    }
}
