<?php

namespace App\Http\Controllers;

use App\Handlers\CompanyHandler;
use config\Constant\Company as CompanyConst;
use AllowDynamicProperties;
use App\Models\Company;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use RuntimeException;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

#[AllowDynamicProperties]
class CompanyController extends Controller {

    /**
     * List all of companies
     *
     * @return JsonResponse
     */
    public function listCompanies(): JsonResponse {
        try {
            $companies = Company::all();
            return response()->json($companies);
        } catch (Exception $e) {
            return response()->json([CompanyConst::ERROR => CompanyConst::COMPANIES_NOT_FOUND_ERROR_MSG], ResponseAlias::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Search company by ids
     *
     * @param Request $request
     * @param CompanyHandler $companyHandler
     * @return JsonResponse
     */
    public function searchCompanies(Request $request, CompanyHandler $companyHandler): JsonResponse {
        try {
            $this->data = json_decode($request->getContent());
            $companyHandler->searchingParameterValidation($this->data);
            $companies = Company::whereIn('id', $this->data->company_ids)->get();
            return response()->json($companies);
        } catch (Exception $e) {
            return response()->json([CompanyConst::ERROR => $e->getMessage()], ResponseAlias::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Create new company
     *
     * @param Request $request
     * @param CompanyHandler $companyHandler
     * @return JsonResponse
     */
    public function createCompany(Request $request, CompanyHandler $companyHandler): JsonResponse {
        try {
            $validatedData = $companyHandler->newCompanyDataValidation($request);
            $newCompany = Company::create($validatedData);
            return response()->json([CompanyConst::MESSAGE => CompanyConst::NEW_COMPANY_ADDED_MSG, 'id' => $newCompany->id], ResponseAlias::HTTP_CREATED);
        } catch (Exception $e) {
            return response()->json([CompanyConst::ERROR =>  CompanyConst::NEW_COMPANY_ADDED_ERROR_MSG . ' | ' . $e->getMessage()], ResponseAlias::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Update company
     *
     * @param int $companyId
     * @param Request $request
     * @param CompanyHandler $companyHandler
     * @return JsonResponse
     */
    public function updateCompany(int $companyId, Request $request, CompanyHandler $companyHandler): JsonResponse {
        try {

            $company = Company::where('id', $companyId)->first();
            if($company === null) {
                throw new RuntimeException(CompanyConst::COMPANY_NOT_EXIST_ERROR_MSG);
            }

            $validatedData = $companyHandler->updateCompanyDataValidation($companyId, $request);
            $company->update($validatedData);
            return response()->json([CompanyConst::MESSAGE => CompanyConst::COMPANY_UPDATE_MSG]);
        } catch (Exception $e) {
            return response()->json([CompanyConst::ERROR =>  CompanyConst::COMPANY_UPDATE_ERROR_MSG . ' | ' . $e->getMessage()], ResponseAlias::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * List companies by activities
     *
     * @param CompanyHandler $companyHandler
     * @return \Illuminate\Foundation\Application|Response|JsonResponse|Application|ResponseFactory
     */
    public function listCompaniesByActivity(CompanyHandler $companyHandler): \Illuminate\Foundation\Application|Response|JsonResponse|Application|ResponseFactory {
        try {
            $html = $companyHandler->createCompanyTableByActivity();
            return response($html, ResponseAlias::HTTP_OK)->header('Content-Type', 'text/html');
        } catch (Exception $e) {
            return response()->json([CompanyConst::ERROR => $e->getMessage()], ResponseAlias::HTTP_UNPROCESSABLE_ENTITY);
        }
    }
}
