<?php

namespace config\Constant;

class Company {

    public const ERROR = 'error';
    public const MESSAGE = 'message';
    public const COMPANIES_NOT_FOUND_ERROR_MSG = 'Companies not found';
    public const INVALID_PARAMETERS_ERROR_MSG = 'Invalid parameters';
    public const NEW_COMPANY_ADDED_MSG = 'New company added successful';
    public const NEW_COMPANY_ADDED_ERROR_MSG = 'New company added not successful';
    public const COMPANY_NOT_EXIST_ERROR_MSG = 'Company is not exist';
    public const COMPANY_UPDATE_MSG = 'Company updated is successful';
    public const COMPANY_UPDATE_ERROR_MSG = 'Company updated is not successful';
    public const ACTIVITIES_NOT_FOUND_ERROR_MSG = 'Activities not found';
    public const QUERY_ERROR_MSG = 'An error occurred during the query';
}
