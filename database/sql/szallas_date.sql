SET SESSION cte_max_recursion_depth = 100000;

SELECT actual_date, COALESCE(GROUP_CONCAT(name SEPARATOR ', '), NULL) AS company_name
FROM (WITH RECURSIVE dates(Date) AS
    (
        SELECT '2001-01-01' as Date
        UNION ALL
        SELECT DATE_ADD(Date, INTERVAL 1 day) FROM dates WHERE Date < CURDATE()
    )
SELECT DATE(Date) AS actual_date FROM dates) AS all_date
LEFT JOIN company AS c ON (all_date.actual_date = c.foundation_date)
GROUP BY actual_date;