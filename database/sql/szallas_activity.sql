SET @sql = NULL;

SELECT
GROUP_CONCAT(DISTINCT CONCAT(
  '
   CASE WHEN activity = "', activity, '" THEN name ELSE "" END 
  AS ', "', activity, '")
)
INTO @sql
FROM company;


SET @sql = CONCAT('SELECT ', @sql, ' FROM company');

PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;