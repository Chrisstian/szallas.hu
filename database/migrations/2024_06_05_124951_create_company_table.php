<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('company', function (Blueprint $table) {
            $table->id()->unsigned()->autoIncrement()->primary();
            $table->string('name', 255);
            $table->string('registration_number', 16);
            $table->date('foundation_date');
            $table->string('country', 128);
            $table->string('zip_code', 16);
            $table->string('city', 128);
            $table->string('street_address', 255);
            $table->float('latitude', 16);
            $table->float('longitude', 16);
            $table->string('owner', 128);
            $table->integer('number_of_employee')->unsigned();
            $table->string('activity', 64);
            $table->tinyInteger('active')->unsigned();
            $table->string('email', 128);
            $table->string('password');
            $table->timestamp('created_at')->nullable()->default(NULL);
            $table->timestamp('updated_at')->nullable()->default(NULL)->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('company');
    }
};
