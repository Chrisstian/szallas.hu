<?php

use App\Http\Controllers\CompanyController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::get('/companies', [CompanyController::class, 'listCompanies']);
Route::get('/companies-by-id', [CompanyController::class, 'searchCompanies']);
Route::post('/companies', [CompanyController::class, 'createCompany']);
Route::patch('/companies/{companyId}', [CompanyController::class, 'updateCompany']);

Route::get('/companies-by-activity', [CompanyController::class, 'listCompaniesByActivity']);
